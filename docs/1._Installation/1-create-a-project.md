# Create a project

To create a project in order to instantiate a new Grafana instance, go to the [Web Services Portal](https://webservices-portal.web.cern.ch/app-catalogue) and fill in the form.

For creating a Grafana instance for development purposes, consider using the [Web Services Portal Dev](https://webservices-portal-dev.web.cern.ch/app-catalogue) instead.

!!! warning
    Notice that both Web Services Portal Dev and app-cat-stg.cern.ch (where the Grafana instance is deployed) are development deployments that may be unavailable at any moment.


![image.png](../assets/image.png)

Once the project is created, if you go to its management page, you will find a link `Access project through OKD console` from where you can access the OKD console to continue configuring it.
