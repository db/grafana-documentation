# Management, ownership and project lifecycle

For the ownership and project lifecycle documentation, refer to the [PaaS documentation](https://paas.docs.cern.ch/1._Getting_Started/2-lifecycle-policy/) link.

For more management actions, please, refer to the [PaaS documentation](https://paas.docs.cern.ch/1._Getting_Started/3-managing-paas-project/) link.
