# Configure new Datasources

In order to add an new `DataSource`, you can do as follows:

As `administrator`, go to the `Engineering wheel` > `Data Sources`.

![image-10.png](../assets/image-10.png)

In the new window, click on <kbd>Add data source</kbd>

![image-11.png](../assets/image-11.png)

Select the corresponding data source you want to add.

## Add a Datasource which fetches from the DBoD Service

For this example, we will add a new `InfluxDB` datasource, which will fetch data from the [DBoD service](https://dbod.web.cern.ch/).

![image-12.png](../assets/image-12.png)

In the new window, fill the fields with the corresponding data. At minimun, the following fields are required:

* `Name`: Name of the Datasource.
* HTTP:
  * `URL`: URL of the database, including the port. (e.g., `https://dbod-acabezas-influx-001.cern.ch:8089`)
* Auth:
  * `Skip TLS Verify`: To disable certificate validation, must be enabled. This is important, otherwise you will get a `502: Bad Gateway` error.
  
![image-15.png](../assets/image-15.png)

* InfluxDB Details:
  * `Database`: database name.
  * `User`: database user.
  * `Password`: user password.
  * `HTTP Method`: uses to be `GET`.

Rest of the values can be accommodated at your best convenience.

![image-13.png](../assets/image-13.png)

Finally, click on <kbd>Save & test</kbd> And you will get a message like the following:

![image-14.png](../assets/image-14.png)

An example of how the values can be filled is shown below:

![image-16.png](../assets/image-16.png)
