# Edit initial configuration

Users have the possibility of editing the initial manifest in order to provide new configurations. 

!!! warning
    As of now, whenever a user wants to add a new parameter/configuration to the `grafana.ini` file, this change must be done through the Custom Resource YAML manifest. The `grafana.ini` segment is populated from the `spec.config` element, and any explicit change to the `grafana.ini` file will be lost.
    See <https://grafana.com/docs/grafana/latest/administration/configuration/> for a full list of possible configurations to be used under the `spec.config` element.

For example, let's assume a user wants to add a new `[smtp]` configuration to the `grafana.ini` file. The procedure will be as follows:

Under the `Administrator` environment, go to `Operators` and then to `Installed Operators`. Select your project and go to the `Grafana` tab:

![image-23.png](../assets/image-23.png)

Select your grafana instance:

![image-24.png](../assets/image-24.png)

And now go to the `YAML` tab:

![image-25.png](../assets/image-25.png)

In here, under `spec`, you can edit any of the values made available by the Operator.

![image-26.png](../assets/image-26.png)

For the aforementioned `[smtp]` use case, our YAML must contains the following:

```yaml
spec:
  config:
    analytics:
      check_for_updates: "true"
    auth:
      disable_login_form: "true"
      oauth_auto_login: "true"
    auth.basic:
      enabled: "false"
    ...
    # Our new configuration. Be aware of the indentation
    smtp:
      enabled: "true"
      host: "cernmx.cern.ch:25"
      from_address: "my-awesome-grafana@cern.ch"
    ...
```

!!! warning
  Always quote the boolean values like in the previous example!

Eventually, this new `[smtp]` configuration will be **automagically** populated to the corresponding `grafana.ini`, and **no further action from the user is needed**. It will look as follows:

```text
[analytics]
check_for_updates = true
[auth]
disable_login_form = true
oauth_auto_login = true
...
[smtp]
enabled = true
host = "cernmx.cern.ch:25"
from_address = "my-awesome-grafana@cern.ch"
```

Same applies for all configurations contained under <https://grafana.com/docs/grafana/latest/administration/configuration/>

Finally click <kbd>Save</kbd> to save your edition.

!!! info
    After saving, the Grafana operator will automatically deploy changes by provisioning a new pod with the new edited configuration. It will make these changes effective in some seconds.
    See `Workloads` > `Pods` in the OKD4 console for further info.
