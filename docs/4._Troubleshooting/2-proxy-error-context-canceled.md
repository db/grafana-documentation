# HTTP: proxy error: context canceled

This is a known issue, and it has beed reported at [grafana/grafana#27957](https://github.com/grafana/grafana/issues/27957), and it is due to the use of multiple queries towards a Prometheus datasource.

How the error log looks like:

```bash
...
t=2021-11-05T14:30:28+0000 lvl=eror msg="Data proxy error" logger=data-proxy-log userId=17 orgId=1 uname=xxx path=/api/datasources/proxy/4/render remote_addr=2001:xxxx:xxx:xx::xxx referer="https://<some_url>" error="http: proxy error: context canceled"
...
```

Solution:

The solution proposed is to change the `refId` to a unique value in the json, according to [grafana/grafana#27957 (comment)](https://github.com/grafana/grafana/issues/27957#issuecomment-705383082)
